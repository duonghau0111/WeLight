<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{

    public function index()
    {
        return view('content.login');
    }
    public function login(Request $request)
    {
        // $user = new User();
        $mail = $request->email;
        $password = MD5($request->password);
        $result = DB::table('users')
            ->where('email',$mail)
            ->where('user_password',$password)
            ->first();
        if ($result) {
            // Session::put($user->id,$request->id);
            // Session::put($user->name,$request->users_name);
            // Session::put($user->image,$request->users_image);
            // Session::put($user->bday,$request->b_day);
            // Session::put($user->cday,$request->create_day);
            // Session::put($user->f_name,$request->f_name);
            // Session::put($user->l_name,$request->l_name);
            // Session::put($user->email,$request->email);
            // Session::put($user->phone,$request->phone);
            // Session::put($user->ship_location,$request->ship_location);
            // Session::put($user->city,$request->city_conscious);
            // Session::put($user->country,$request->country);
            // Session::put($user->roles,$request->roles);

            Session::put('userName',$result->userName);
            Session::put('userID',$result->userID);
            Session::put('role',$result->role);
            return Redirect::to('/home');
        }
        else
        {
            Session::put('message','Wrong email or password!');
            return Redirect::to('/login');
        }
    }
}
