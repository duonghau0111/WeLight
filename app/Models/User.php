<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    private $id;
    private $name;
    private $password;
    private $image;
    private $bday;
    private $cday;
    private $f_name;
    private $l_name;
    private $email;
    private $phone;
    private $ship_location;
    private $city;
    private $country;
    private $roles;

    /**
     * @param $id
     * @param $name
     * @param $password
     * @param $image
     * @param $bday
     * @param $cday
     * @param $f_name
     * @param $l_name
     * @param $email
     * @param $phone
     * @param $ship_location
     * @param $city
     * @param $country
     * @param $roles
     */
    public function User($id, $name, $password, $image, $bday, $cday, $f_name, $l_name, $email, $phone, $ship_location, $city, $country, $roles)
    {
        $this->id = $id;
        $this->name = $name;
        $this->password = $password;
        $this->image = $image;
        $this->bday = $bday;
        $this->cday = $cday;
        $this->f_name = $f_name;
        $this->l_name = $l_name;
        $this->email = $email;
        $this->phone = $phone;
        $this->ship_location = $ship_location;
        $this->city = $city;
        $this->country = $country;
        $this->roles = $roles;
    }

    /**
     * @return string
     */
    public function getRememberTokenName(): string
    {
        return $this->rememberTokenName;
    }

    /**
     * @param string $rememberTokenName
     */
    public function setRememberTokenName(string $rememberTokenName): void
    {
        $this->rememberTokenName = $rememberTokenName;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getBday()
    {
        return $this->bday;
    }

    /**
     * @param mixed $bday
     */
    public function setBday($bday): void
    {
        $this->bday = $bday;
    }

    /**
     * @return mixed
     */
    public function getCday()
    {
        return $this->cday;
    }

    /**
     * @param mixed $cday
     */
    public function setCday($cday): void
    {
        $this->cday = $cday;
    }

    /**
     * @return mixed
     */
    public function getFName()
    {
        return $this->f_name;
    }

    /**
     * @param mixed $f_name
     */
    public function setFName($f_name): void
    {
        $this->f_name = $f_name;
    }

    /**
     * @return mixed
     */
    public function getLName()
    {
        return $this->l_name;
    }

    /**
     * @param mixed $l_name
     */
    public function setLName($l_name): void
    {
        $this->l_name = $l_name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getShipLocation()
    {
        return $this->ship_location;
    }

    /**
     * @param mixed $ship_location
     */
    public function setShipLocation($ship_location): void
    {
        $this->ship_location = $ship_location;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param mixed $roles
     */
    public function setRoles($roles): void
    {
        $this->roles = $roles;
    }

    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
