<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
//vai` bu~a xoa'

Route::get('/', 'App\Http\Controllers\AdminController@index');

Route::get('/home', 'App\Http\Controllers\AdminController@index');

Route::get('/product', 'App\Http\Controllers\BooksController@index');

Route::get('/cart', 'App\Http\Controllers\CartController@index');

Route::get('/check-out', 'App\Http\Controllers\CheckoutController@index');

Route::get('/contact', 'App\Http\Controllers\ContactController@index');

Route::get('/login', 'App\Http\Controllers\UserController@index');

Route::post('/dangnhap', 'App\Http\Controllers\UserController@login');
